package com.lr.transfer.config;

import com.lr.transfer.component.ResourceComponent;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 通用配置
 *
 * @author lr
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer {

    public final static String PREVIEW = "/files/";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //css js
        registry.addResourceHandler("/static/lr/**").addResourceLocations("classpath:/static/lr/");
        // 文件访问路径
        registry.addResourceHandler(PREVIEW + "**").addResourceLocations("file:" + ResourceComponent.fileUploadPath);
    }

}