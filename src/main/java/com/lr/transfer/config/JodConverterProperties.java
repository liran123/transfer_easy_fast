package com.lr.transfer.config;

import com.lr.transfer.component.ResourceComponent;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * openOffice 配置
 *
 * @author liran
 * @date 20190517
 */
@ConfigurationProperties("jodconverter")
@Data
public class JodConverterProperties {
    private boolean enabled;
    private String officeHome;
    private String portNumbers = "2002";
    private String workingDir;
    private String templateProfileDir;
    private boolean killExistingProcess = true;
    private long processTimeout = 120000L;
    private long processRetryInterval = 250L;
    private long taskExecutionTimeout = 120000L;
    private int maxTasksPerProcess = 200;
    private long taskQueueTimeout = 30000L;

    public String getOfficeHome() {
        //根据不同系统分别设置
        //office-home: C:\Program Files (x86)\OpenOffice 4  #windows下默认 不用修改
        // #office-home: /opt/openoffice4      #linux 默认 不用修改
        if (ResourceComponent.isWindowsSystem()) {
            this.officeHome = "C:\\Program Files (x86)\\OpenOffice 4";
        } else {
            this.officeHome = "/opt/openoffice4";
        }
        return this.officeHome;
    }

}