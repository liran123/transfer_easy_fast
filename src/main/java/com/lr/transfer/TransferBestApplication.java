package com.lr.transfer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lr
 */
@SpringBootApplication
public class TransferBestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransferBestApplication.class, args);
    }

}
