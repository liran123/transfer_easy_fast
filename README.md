# transfer_easy_fast

#### 介绍
word转换pdf,pdf转换img
pdf转图片，用到多线程转换，大大提升转换速度
另一种方式： [基于docx4j框架 word转换pdf](https://gitee.com/liran123/wrod2pdf)
#### 软件架构
软件架构说明


#### 安装教程

1. [OpenOffice安装和转换乱码解决方案](https://www.cnblogs.com/liran123/p/9846349.html)
2. 原博客地址：https://www.cnblogs.com/liran123/p/10883582.html
3. xxxx

#### 使用说明

1. 先安装 openOffice ,linux乱码解决看 https://www.cnblogs.com/liran123/p/9846349.html
2. application.yml配置里面有启用和停用配置，未安装openOffice设置为false才能不影响项目启动
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)